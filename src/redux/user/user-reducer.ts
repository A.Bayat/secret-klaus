import { userActionType } from "./user-action.enum";
import { IUser, IUserAction } from "./user-types";

const INITIAL_STATE: IUser = {
  uid: undefined,
  email: undefined,
  displayName: undefined,
  emailVerified: undefined
};

const updateSignInUser = (
  state: IUser,
  payload: firebase.auth.UserCredential
) => {
  return {
    ...state,
    uid: payload.user?.uid,
    email: payload.user?.email,
    displayName: payload.user?.displayName,
    emailVerified: payload.user?.emailVerified
  };
};
export const userReducer = (
  state: IUser = INITIAL_STATE,
  action: IUserAction
) => {
  switch (action.type) {
    case userActionType.SET_CURRENT_USER:
      return {
        ...state,
        uid: action.payload.uid,
        email: action.payload.email,
        displayName: action.payload.displayName,
        emailVerified: action.payload.emailVerified
      };
    case userActionType.SET_EMAIL_VERIFIED:
      if (state.uid !== action.payload.uid) {
        return {
          ...state
        };
      }
      return {
        ...state,
        emailVerified: action.payload.emailVerified
      };
    case userActionType.SIGN_UP:
      return {
        ...state
      };
    case userActionType.SIGN_OUT:
      return {
        ...state
      };
    case userActionType.SIGN_IN:
      return updateSignInUser(state, action.payload);
    default:
      return state;
  }
};
