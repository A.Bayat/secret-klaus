import { IUser } from "./user/user-types";

export interface StoreState {
  user: IUser;
}
