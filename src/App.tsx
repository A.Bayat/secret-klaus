import React, { useState, useEffect } from "react";
import Header from "./components/header/header.component";
import { connect } from "react-redux";
import { setCurrentUser } from "./redux/user/user-action";

import SignInOrUp from "./pages/sing-in-up/sign-in-up.component";
import ForgotPassword from "./components/forgot-password/forgot-password.component";
import HomePage from "../src/pages/homepage/homepage.component";
import { ReactComponent as LoadingIcon } from "../src/assets/Bars-1s-100px.svg";
import { Switch, Route, Redirect } from "react-router-dom";
import "./App.scss";
import { auth } from "./firebase/firebase";
import User from "./firebase/models/user";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faCheckSquare,
  faEnvelope,
  faUser,
  faLockOpen,
  faLock
} from "@fortawesome/free-solid-svg-icons";

library.add(fab, faCheckSquare, faEnvelope, faUser, faLockOpen, faLock);

// import { auth } from "./firebase/firebase.util";
interface appProps {
  setCurrentUser: Function;
}

function App(props: appProps, ownProps: appProps) {
  const [loading, setLoading] = useState<boolean>(false);
  const [{ id, currentUser }, setCurrentUser] = useState<{
    id: string | undefined;
    currentUser: firebase.firestore.DocumentData | undefined;
  }>({ id: "", currentUser: undefined });
  // console.log(Object.keys(props));
  useEffect(() => {
    setLoading(true);

    return auth().onAuthStateChanged(async user => {
      if (user) {
        console.log("verified ", user.emailVerified);
        const fireUser = new User(user);
        const userRef = await fireUser.createUserProfileDocument();
        userRef?.onSnapshot(async snapShot => {
          if (snapShot.exists && user.emailVerified) {
            console.log("snapshot", snapShot.data());
            setCurrentUser({ id: snapShot.id, currentUser: snapShot.data() });

            props.setCurrentUser(snapShot.data());
            setLoading(false);
          } else {
            setCurrentUser({ id: undefined, currentUser: undefined });
            setLoading(false);
          }
        });
      } else {
        setCurrentUser({ id: undefined, currentUser: undefined });
        setLoading(false);
      }
    });
  }, [id]);

  return (
    <div className="App">
      <Header />
      {loading ? (
        <div className="popup">
          <LoadingIcon className="loadingIcon lg" />
        </div>
      ) : (
        undefined
      )}
      <Switch>
        <Route path="/login" />
        <Route path="/sign-in-up" component={SignInOrUp}>
          {id ? <Redirect to="/" /> : ""}
        </Route>
        <Route path="/forgot-password" component={ForgotPassword}></Route>
        <Route path="/" component={HomePage} />
      </Switch>
    </div>
  );
}

// const mapDispatchToProps = (dispatch: any) => {
//   return {
//     setUser: (user: any) => dispatch(setCurrentUser(user))
//   };
// };

export default connect(null, { setCurrentUser })(App);
