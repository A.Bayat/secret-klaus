import React from "react";
import { connect, ConnectedProps } from "react-redux";
import { signIn, setCurrentUser } from "../../redux/user/user-action";
import "../sign-in/sign-in.style.scss";
import FormInput from "../form-input/form-input.component";
import { Link } from "react-router-dom";
import { signInWithGoogle } from "../../firebase/firebase";
import { ReactComponent as GoogleLogo } from "../../assets/icons8-google.svg";
import { ReactComponent as FaceBookLogo } from "../../assets/iconmonstr-facebook-6.svg";
import GenericButton from "../generic-button/generic-button.component";
import Checkbox from "../generic-checkbox/generic-checkbox.component";
import { auth } from "../../firebase/firebase";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ISignIn } from "../../redux/user/user-types";

interface SignInState {
  email: string;
  password: string;
  loading: boolean;
  error?: string | undefined;
}

interface SingInProp {
  id: string;
}

// type PropsFromRedux = ConnectedProps<typeof connector>;
// type Props = PropsFromRedux & { id: string; signIn: Function };
class SingIn extends FormInput<SignInState> {
  constructor(props: SingInProp) {
    super(props);
    this.state = { email: "", password: "", loading: false };
  }

  async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    this.setState({ loading: true });
    event.preventDefault();
    const { email, password } = this.state;
    try {
      // const result = await auth().signInWithEmailAndPassword(email, password);
      this.setState({ loading: false });

      this.setState({ email: "", password: "" });
    } catch (error) {
      console.log(error.message);
      this.setState({ loading: false });
    }
  }

  // handelChange = (event: React.FormEvent<HTMLInputElement>): void => {
  //   const { name, value } = event.currentTarget;
  //   this.setState(state => ({ ...state, [name]: value }));
  // };

  render() {
    const { loading } = this.state;
    return (
      <div className="sign-in">
        <h2>Have an account?</h2>
        <span>Sign in with your email and password</span>
        <form onSubmit={e => this.handleSubmit(e)}>
          <FormInput
            children={<FontAwesomeIcon icon={"envelope"} size={"1x"} />}
            type="email"
            name="email"
            id="sign-up-email"
            value={this.state.email}
            handelChange={this.handelChange}
            label="Email"
            error={this.state.error}
          />
          <FormInput
            children={<FontAwesomeIcon icon={"lock-open"} size={"1x"} />}
            type="password"
            name="password"
            id="sign-up-password"
            value={this.state.password}
            handelChange={this.handelChange}
            label="Password"
            error={this.state.error}
          />
          <div className="terms-reset">
            <Checkbox
              id="unchecked"
              label="Keep me logged in"
              hasError={false}
              type="default"
              indeterminate={false}
              disabled={false}
              error={this.state.error}
            />
            <span>
              <Link to="/forgot-password">Forgot your password?</Link>
            </span>
          </div>
          <div className="submit">
            <GenericButton
              disabled={loading ? true : false}
              async={this.state.loading}
              className="btn sign-in primary"
            >
              Sign in
            </GenericButton>
          </div>
          <div className="other-account">
            <span onClick={signInWithGoogle}>
              <div className="third-party">
                <GoogleLogo type="submit" className="logo" />
                <span></span>
              </div>
            </span>
            <span>
              <div className="third-party">
                <FaceBookLogo type="submit" className="logo" />
                <span></span>
              </div>
            </span>
          </div>
        </form>
      </div>
    );
  }
}

// const connector = connect<SignInState, SingInProp>(null, { signIn });
// export default connector(SingIn);
export default SingIn;
