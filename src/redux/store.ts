import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import logger from "redux-logger";

import rootReducer from "./root-reducer";

const middleWares = [logger, thunk];

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers =
  (window["__REDUX_DEVTOOLS_EXTENSION_COMPOSE__"] as typeof compose) || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middleWares))
);

export default store;
