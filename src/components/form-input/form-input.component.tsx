import React from "react";
import "./form-input.style.scss";

export interface FormInputProps {
  handelChange?: (event: React.FormEvent<HTMLInputElement>) => void;
  label?: string | undefined;
  value?: string;
  type?: string;
  name?: string;
  id?: string;
  children?: JSX.Element | undefined;
  autocomplete?: string;
  error?: string;
}

class FormInput<S> extends React.Component<FormInputProps, S> {
  handelChange = (event: React.FormEvent<HTMLInputElement>): void => {
    const { name, value } = event.currentTarget;
    this.setState(state => ({ ...state, [name]: value }));
    console.log("handel change");
  };
  inputLabel = (
    error: string,
    label: string | undefined,
    children: JSX.Element | undefined,
    valueLength: number | undefined,
    type: string
  ) => {
    if (label) {
      if (!children) {
        return (
          <label
            className={`form-input-label ${valueLength ? "shrink" : ""} ${
              type === "checkbox" ? "checkboxLabel" : ""
            }`}
          >
            {label}
          </label>
        );
      } else {
        return (
          <label
            className={`form-input-label ${
              valueLength ? "shrink-above-icon" : ""
            } ${error ? "error" : ""} ${
              type === "checkbox" ? "checkboxLabel" : ""
            }`}
          >
            {`${label} ${error}`}
          </label>
        );
      }
    }
  };
  render() {
    const {
      value,
      label,
      children,
      autocomplete,
      type,
      error = "",
      handelChange,
      ...otherProps
    } = this.props;
    return (
      <div className="group">
        <div className={type === "checkbox" ? "checkboxAndLabel" : ""}>
          {children}
          <input
            id={otherProps.name}
            autoComplete={autocomplete}
            className={`${children ? "form-input with-icon" : "form-input"} ${
              error ? "error" : ""
            }`}
            type={type}
            onChange={handelChange}
            {...otherProps}
          />
          {this.inputLabel(
            error,
            label,
            children,
            value?.length,
            type ? type : ""
          )}
        </div>
      </div>
    );
  }
}

export default FormInput;
