import React from "react";
import "./forgot-password.style.scss";
import FormInput from "../../components/form-input/form-input.component";
import GenericButton from "../generic-button/generic-button.component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { auth } from "../../firebase/firebase";

interface ForgotPasswordState {
  email: string;
  code: string;
  error?: string | undefined;
  loading: boolean;
}

export default class ForgotPassword extends FormInput<ForgotPasswordState> {
  constructor(props: any) {
    super(props);
    this.state = { email: "", code: "", loading: false };
  }

  async sendPasswordResetEmail(event: React.FormEvent<HTMLFormElement>) {
    console.log(this.state.email);
    event.preventDefault();
    this.setState({ loading: true });
    if (!this.state.email) {
      alert("Not a valid email");
      this.setState({ loading: false });
    }
    try {
      await auth().sendPasswordResetEmail(this.state.email);
      alert("Reset password instruction sent");

      this.setState({ loading: false });
      this.setState({ email: "" });
    } catch (error) {
      console.log("Something went wrong");
      this.setState({ loading: false });
    }
  }
  render() {
    const { email, loading } = this.state;
    return (
      <div className="forgot-password">
        <h2>Forgot Your Password</h2>
        <span>Please enter the email address last time used to sign in</span>
        <form autoComplete="off" onSubmit={e => this.sendPasswordResetEmail(e)}>
          <FormInput
            children={<FontAwesomeIcon icon={"envelope"} size={"1x"} />}
            type="email"
            name="email"
            id="forgot-password"
            value={email}
            handelChange={this.handelChange}
            label="Email"
            error={this.state.error}
          />
          <GenericButton
            disabled={loading ? true : false}
            async={loading}
            className="btn forgot-password primary"
          >
            Next
          </GenericButton>
        </form>
      </div>
    );
  }
}
