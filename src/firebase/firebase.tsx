import firebase from "firebase/app";

import "firebase/auth";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyAokgOAb60aFQRxm8FNfy_Mz3QWkp5r2KE",
  authDomain: "secret-klaus.firebaseapp.com",
  databaseURL: "https://secret-klaus.firebaseio.com",
  projectId: "secret-klaus",
  storageBucket: "secret-klaus.appspot.com",
  messagingSenderId: "453508218461",
  appId: "1:453508218461:web:0563eaf371caa8d50a97b4",
  measurementId: "G-83JXTTNNQ5"
};

export class FireBase {
  private static instance: firebase.app.App;
  private static authInstance: firebase.auth.Auth;
  private static fireStoreInstance: firebase.firestore.Firestore;
  private static googleAuthProviderInstance =
    firebase.auth.GoogleAuthProvider_Instance;
  static getInstance(): firebase.app.App {
    if (!FireBase.instance) {
      FireBase.instance = firebase.initializeApp(config);
    }
    return FireBase.instance;
  }

  static getAuthInstance(): firebase.auth.Auth {
    if (!FireBase.authInstance) {
      FireBase.authInstance = FireBase.getInstance().auth();
    }
    return FireBase.authInstance;
  }

  static getFireStoreInstance(): firebase.firestore.Firestore {
    if (!FireBase.fireStoreInstance) {
      FireBase.fireStoreInstance = FireBase.getInstance().firestore();
    }
    return FireBase.fireStoreInstance;
  }

  static getGoogleAuthProviderInstance(): firebase.auth.GoogleAuthProvider_Instance {
    if (!FireBase.googleAuthProviderInstance) {
      FireBase.googleAuthProviderInstance = firebase.auth.GoogleAuthProvider;
    }
    return new FireBase.googleAuthProviderInstance();
  }
  static signInWithGoogle(): Promise<firebase.auth.UserCredential> {
    const provider = FireBase.getGoogleAuthProviderInstance();
    provider.setCustomParameters({ prompt: "select_account" });
    return FireBase.getAuthInstance().signInWithPopup(provider);
  }
}
export const auth = FireBase.getAuthInstance;
export const signInWithGoogle = FireBase.signInWithGoogle;
export const fireStore = FireBase.getFireStoreInstance;
