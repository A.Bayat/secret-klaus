import { userActionType } from "./user-action.enum";
import { Dispatch } from "redux";
import { auth } from "../../firebase/firebase";

import {
  IUser,
  IActionSetCurrentUser,
  IActionSetEmailVerified,
  IActionSignIn,
  ISignIn,
  IEmailVerified
} from "./user-types";
import { StoreState } from "../store-type";

export const setCurrentUser = (user: IUser) => {
  return (dispatch: Dispatch) => {
    dispatch<IActionSetCurrentUser>({
      type: userActionType.SET_CURRENT_USER,
      payload: user
    });
  };
};

export const setEmailVerified = (emailVerified: IEmailVerified) => {
  return (dispatch: Dispatch) => {
    dispatch<IActionSetEmailVerified>({
      type: userActionType.SET_EMAIL_VERIFIED,
      payload: emailVerified
    });
  };
};

export const signUp = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    dispatch({ type: userActionType.SIGN_UP });
  };
};

export const signOut = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    dispatch({ type: userActionType.SIGN_OUT });
  };
};

export const signIn = (signInCredentials: ISignIn) => {
  return async (dispatch: Dispatch, getState: () => StoreState) => {
    const result: firebase.auth.UserCredential = await auth().signInWithEmailAndPassword(
      signInCredentials.email,
      signInCredentials.password
    );
    dispatch<IActionSignIn>({
      type: userActionType.SIGN_IN,
      payload: result
    });
  };
};
