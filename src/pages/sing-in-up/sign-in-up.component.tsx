import React, { useState } from "react";
// import { ReactComponent as LoadingIcon } from "../../assets/Bars-1s-100px.svg";
import "./sign-in-up.style.scss";
import SingIn from "../../components/sign-in/sign-in.component";
import SingUp from "../../components/sign-up/sign-up.component";
import GenericButton from "../../components/generic-button/generic-button.component";

const SignInOrUp: React.FC<null> = () => {
  const [state, setState] = useState({ signIn: true, signUp: false });

  return (
    <React.Fragment>
      <div className="sign-in-up toggle">
        <div className="buttons">
          <GenericButton
            type={undefined}
            onClick={() => setState({ signIn: true, signUp: false })}
            active={state.signIn}
            // disabled={loading ? true : false}
            // async={this.state.loading}
            className="btn sign-in-up"
          >
            Sign in
          </GenericButton>
          <GenericButton
            type={undefined}
            active={state.signUp}
            onClick={() => setState({ signIn: false, signUp: true })}
            // disabled={loading ? true : false}
            // async={this.state.loading}
            className="btn sign-in-up"
          >
            Sign up
          </GenericButton>
        </div>
        {state.signIn ? (
          <SingIn id="signIn-component" />
        ) : (
          <SingUp id="signUp-component" />
        )}
      </div>
      <div className="sign-in-up both"></div>
    </React.Fragment>
  );
};

export default SignInOrUp;
