import React from "react";

import "./generic-checkbox.style.scss";

interface checkboxProps {
  label: string | undefined;
  id: string;
  hasError: boolean;
  indeterminate: boolean;
  type: "default" | "switch";
  disabled?: boolean;
  error?: string | undefined;
}
export default class Checkbox extends React.Component<checkboxProps, {}> {
  render() {
    const {
      id,
      label,
      type,
      indeterminate,
      hasError,
      disabled,
      ...inputProps
    } = this.props;
    const checkboxClassname = `
      m-checkbox
      ${type === "switch" && "m-checkbox--switch"}
      ${hasError && "m-checkbox--has-error"}
    `;

    const inputClassname = `
      m-checkbox__input
      ${type === "switch" && "m-checkbox--switch__input"}
      ${hasError && "m-checkbox--has-error__input"}
    `;

    const labelClassname = `
      m-checkbox__label
      ${type === "switch" && "m-checkbox--switch__label"}
    `;
    return (
      <div className={checkboxClassname}>
        <input
          type="checkbox"
          className={inputClassname}
          disabled={disabled}
          // ref={el => (this.selector = el)}
          id={id}
          {...inputProps}
        />
        <label className={labelClassname} htmlFor={id}>
          {label}
        </label>
      </div>
    );
  }
}
