import React from "react";

import "./sign-up.style.scss";
import FormInput from "../../components/form-input/form-input.component";
import Checkbox from "../../components/generic-checkbox/generic-checkbox.component";
import GenericButton from "../generic-button/generic-button.component";
import { auth } from "../../firebase/firebase";
import User from "../../firebase/models/user";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export interface SignUpState {
  displayName: string;
  email: string;
  password: string;
  confirmPassword: string;
  loading: boolean;
  error?: string | undefined;
}
interface SingUpProp {
  id: string;
}

class SignUp extends FormInput<SignUpState> {
  constructor(props: any) {
    super(props);
    this.state = {
      displayName: "",
      confirmPassword: "",
      email: "",
      password: "",
      loading: false
    };
  }

  async handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    this.setState({ loading: true });
    const { displayName, email, password, confirmPassword } = this.state;
    if (password !== confirmPassword) {
      alert("password don't match");
      this.setState({ loading: false });

      return;
    }
    try {
      const { user } = await auth().createUserWithEmailAndPassword(
        email,
        password
      );

      if (user) {
        const fireUser = new User(user, { displayName });
        await fireUser.createUserProfileDocument();
        await user.sendEmailVerification();
        this.setState({ loading: false });

        this.setState({
          displayName: "",
          confirmPassword: "",
          email: "",
          password: ""
        });
      }
      console.log("email verified: ", user?.emailVerified);
    } catch (error) {
      this.setState({ loading: false });
    }
  }

  // handelChange = (event: React.FormEvent<HTMLInputElement>): void => {
  //   const { name, value } = event.currentTarget;
  //   this.setState(state => ({ ...state, [name]: value }));
  // };
  render() {
    const { loading } = this.state;
    return (
      <div className="sign-up">
        <h2>Don't have an account?</h2>
        <span>Sign up with your email and password</span>
        <form autoComplete="off" onSubmit={e => this.handleSubmit(e)}>
          <FormInput
            children={<FontAwesomeIcon icon={"user"} size={"1x"} />}
            type="text"
            name="displayName"
            id="sign-up-displayName"
            value={this.state.displayName}
            handelChange={this.handelChange}
            label="Display Name"
            error={this.state.error}
          />
          <FormInput
            children={<FontAwesomeIcon icon={"envelope"} size={"1x"} />}
            type="email"
            name="email"
            id="sign-up-email"
            value={this.state.email}
            handelChange={this.handelChange}
            label="Email"
            error={this.state.error}
          />
          <FormInput
            children={<FontAwesomeIcon icon={"lock-open"} size={"1x"} />}
            type="password"
            name="password"
            id="sign-up-password"
            value={this.state.password}
            handelChange={this.handelChange}
            label="Password"
            error={this.state.error}
          />
          <FormInput
            children={<FontAwesomeIcon icon={"lock"} size={"1x"} />}
            type="password"
            name="confirmPassword"
            id="sign-up-confirmPassword"
            value={this.state.confirmPassword}
            handelChange={this.handelChange}
            label="Confirm Password"
            error={this.state.error}
          />
          <Checkbox
            id="unchecked"
            label="I've read and accept Terms of Use"
            hasError={true}
            type="default"
            indeterminate={false}
            disabled={false}
          />
          <div className="submit">
            <GenericButton
              disabled={loading ? true : false}
              async={loading}
              className="btn sign-up primary"
            >
              Sign up
            </GenericButton>
          </div>
        </form>
      </div>
    );
  }
}

export default SignUp;
