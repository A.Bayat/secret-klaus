import { fireStore } from "../firebase";
import { User as fireUser, firestore } from "firebase";

interface additionalData {
  displayName: string;
}

export default class User {
  fireUser: fireUser | null;
  additionalData: additionalData | undefined;

  constructor(
    authUser: fireUser | null,
    additionalData?: additionalData | undefined
  ) {
    this.fireUser = authUser;
    this.additionalData = additionalData;
  }

  async createUserProfileDocument(): Promise<
    firestore.DocumentReference<firestore.DocumentData> | undefined
  > {
    if (!this.fireUser) return undefined;
    const userRef = fireStore().doc(`users/${this.fireUser.uid}`);
    const snapShot = await userRef.get();
    if (!snapShot.exists) {
      const { email, displayName, uid } = this.fireUser;
      const createAt = new Date();
      try {
        await userRef.set({
          email,
          displayName,
          uid,
          createAt,
          ...this.additionalData
        });
      } catch (error) {
        console.log("Error creating user ", error.message);
      }
    }
    return userRef;
  }
}
