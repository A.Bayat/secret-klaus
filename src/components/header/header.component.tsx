import React from "react";
import { connect } from "react-redux";
import "./header.style.scss";
import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "../../../src/assets/logo.svg";
import { ReactComponent as LeftLogo } from "../../../src/assets/left-1.1s-24px.svg";
// import { auth } from "../../firebase/firebase.util";
import { auth } from "../../firebase/firebase";
import store from "../../redux/store";
interface headerProps {
  user?: string | undefined;
  currentUser: any;
}
function Header(props: headerProps) {
  console.log(arguments);
  console.log("ffe", Object.keys(props.currentUser.user));
  return (
    <div className="header">
      <div className="logo-container">
        <Link className="logo" to="/">
          <Logo className="logo" />
        </Link>
      </div>
      <div className="options">
        {props.currentUser.user.currentUser ? (
          <React.Fragment>
            <Link
              className="option btn primary"
              onClick={async () => await auth().signOut()}
              to="/"
            >
              Sing out {}
            </Link>
            <Link className="option btn primary" to="/profile">
              {/* {`Hi ${props.user.split(" ").shift()}`} */}
              {`Hi ${props.currentUser.user.currentUser.displayName}`}
            </Link>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Link className="option" to="/sign-in-up">
              <LeftLogo />
            </Link>
          </React.Fragment>
        )}
      </div>
    </div>
  );
}

const mapStateToProps = (state: headerProps) => ({
  currentUser: { ...state }
});

export default connect(mapStateToProps)(Header);
