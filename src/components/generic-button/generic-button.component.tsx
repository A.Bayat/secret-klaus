import React from "react";

import "./generic-button.style.scss";
import { ReactComponent as LoadingIcon } from "../../assets/Bars-1s-25px.svg";

interface buttonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children?: JSX.Element | string;
  type?: "submit" | "reset" | "button" | undefined;
  async?: boolean;
  active?: boolean | undefined;
  className: string;
}

const GenericButton = ({
  type = "submit",
  children,
  active,
  async,
  className,
  ...otherProps
}: buttonProps) => {
  return (
    <button
      type={type}
      className={`${className} ${active ? "activated" : ""}`}
      {...otherProps}
    >
      {children}
      {async ? <LoadingIcon className={"button loading"} /> : null}
    </button>
  );
};

export default GenericButton;
