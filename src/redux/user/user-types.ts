import { userActionType } from "./user-action.enum";

// user data type
export interface IUser {
  uid: string | null | undefined;
  email: string | null | undefined;
  displayName: string | null | undefined;
  emailVerified: boolean | null | undefined;
}

export interface IEmailVerified {
  uid: string | undefined;
  emailVerified: boolean | undefined;
}

export interface ISignIn {
  email: string;
  password: string;
}

// user actions type
export interface IActionSetCurrentUser {
  type: userActionType.SET_CURRENT_USER;
  payload: IUser;
}

export interface IActionSetEmailVerified {
  type: userActionType.SET_EMAIL_VERIFIED;
  payload: IEmailVerified;
}

export interface IActionSignUp {
  type: userActionType.SIGN_UP;
}

export interface IActionSignOut {
  type: userActionType.SIGN_OUT;
}
export interface IActionSignIn {
  type: userActionType.SIGN_IN;
  payload: firebase.auth.UserCredential;
}

// Combined user actions type
export type IUserAction =
  | IActionSetCurrentUser
  | IActionSetEmailVerified
  | IActionSignUp
  | IActionSignOut
  | IActionSignIn;
