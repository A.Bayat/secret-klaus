import { combineReducers } from "redux";
import { userReducer } from "./user/user-reducer";
import { StoreState } from "./store-type";

export default combineReducers<StoreState>({
  user: userReducer
});
